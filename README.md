# Commands( to run the program )#


### Generate the Key ###

* ./kem-enc -g TempKey/testkey


### Encryption ###

* ./kem-enc -e -i file -o encfile -k TempKey/testkey.pub


### Decryption ###

* ./kem-enc -d -i encfile -o decfile -k TempKey/testkey