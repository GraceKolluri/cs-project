/* Interface for simple pseudo-random function based on HMAC */
 
#pragma once
#include <stddef.h> 

int setSeed(unsigned char* entropy, size_t len);
int random_Bytes(unsigned char* outBuf, size_t len);
