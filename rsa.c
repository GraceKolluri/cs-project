#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "rsa.h"
#include "prf.h"

#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,Buf_er,len) mpz_import(x,len,-1,1,0,0,Buf_er)
#define Z2BYTES(Buf_er,len,x) mpz_export(Buf_er,&len,-1,1,0,0,x)


int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* Buf_er = malloc(len);
	
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(Buf_er,len,x);
	fwrite(Buf_er,1,len,f);
	Set_Num_Mem(Buf_er,0,len);
	free(Buf_er);
	return 0;
}
int Rep_File(FILE* f, mpz_t x)
{
	size_t i,len=0;
		for (i = 0; i < 8; i++) {
		unsigned char b;
		Read_Key_F(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* Buf_er = malloc(len);
	Read_Key_F(Buf_er,1,len,f);
	BYTES2Z(x,Buf_er,len);
		Set_Num_Mem(Buf_er,0,len);
	free(Buf_er);
	return 0;
}


void Prime_Large_Gen( mpz_t bign, int keySize )
{
    NEWZ(TempKey);
    unsigned char *rnd = malloc(keySize);

    while ( !mpz_cmp_ui( bign, 0) )
    {
		random_Bytes( rnd, keySize/2 );
		BYTES2Z( TempKey, rnd, keySize/2 );
		if ( ISPRIME(TempKey) )
			mpz_set( bign, TempKey );
	}
    free(rnd);
}


int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initialize(K);

    /* Generate 2 prime numbers, p and q. Calculate n = p * q */
    Prime_Large_Gen ( K->p, keyBits );
    Prime_Large_Gen ( K->q, keyBits );
    mpz_mul ( K->n, K->p, K->q );

    
    NEWZ(p1); mpz_sub_ui ( p1, K->p, 1 );
    NEWZ(q1); mpz_sub_ui ( q1, K->q, 1 );
    NEWZ(phi); mpz_mul ( phi, p1, q1 );

    
    NEWZ (theGcd);
    int keep_trying = 1;
    unsigned char *cnt = malloc(keyBits);

    while ( keep_trying == 1 )
    {
        random_Bytes ( cnt, keyBits );
        BYTES2Z( K->e, cnt, keyBits );
		mpz_gcd( theGcd, phi, K->e );
        if ( mpz_cmp_ui(theGcd, 1) == 0 )
        {
            keep_trying = 0;
            free(cnt);
        }
    }

    
    mpz_invert ( K->d, K->e, phi );

	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
    NEWZ(out);
    NEWZ(num);

    BYTES2Z(num, inBuf, len);

    /* encrypt */
    mpz_powm_sec (out, num, K->e, K->n);

    /* Encrypted content */
    Z2BYTES(outBuf, len, out);

    return len;
}

size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
    NEWZ(out);
    NEWZ(num);

    BYTES2Z(num, inBuf, len);

    /* decrypt */
    mpz_powm_sec (out, num, K->d, K->n);

    /* Decrypted content */
    Z2BYTES(outBuf, len, out);

    return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initialize(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	
	Find_File(f,K->n);
	Find_File(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	Find_File(f,K->n);
	Find_File(f,K->e);
	Find_File(f,K->p);
	Find_File(f,K->q);
	Find_File(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initialize(K); 
	Rep_File(f,K->n);
	Rep_File(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initialize(K);
	Rep_File(f,K->n);
	Rep_File(f,K->e);
	Rep_File(f,K->p);
	Rep_File(f,K->q);
	Rep_File(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
		mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			Set_Num_Mem(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	
	return 0;
}
