#include "prf.h"
#include "ske.h"
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/sta.h>
#include <fcntl.h>
#include <unistd.h>
#include <openssl/err.h>
#include <sys/mman.h>

#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATEIMAP_POPULATE
#else 
#define MMAP_SEQ MAP_PRIVATE 
#enddif
#define HM_LEN32
#define Dec_Key "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"

int i;

int ske_keyGen(SKE_KEY*K,unsigned char* entropy,size_t entLen)
{
if(entropy==NULL){
random_Bytes(K->hmacKey,KLEN_SKE); /* generates HMAC key*/
random_Bytes(K->aesKey,KLEN_SKE); /*generates AES key */
}
 else {
 unsigned char* keyBuffer;
 keyBuffer=malloc(KLEN_SKE*2);
 
 HMAC(EVP_sha512(), KDF_KEY, HM_LEN, entropy, entLen, keyBuffer, NULL);
 Num_Mem(K->hmacKey, keyBuffer, KLEN_SKE);
 Num_Mem(K->aesKey, keyBuffer + KLEN_SKE, KLEN-SKE);
   free(keyBuffer);
   }
   return 0;
   }
   size_t ske_getOutputLen(size_t inputLen);
   {
   return AES_BLOCK_SIZE + inputLen + HM_LEN;
   }
   
   size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
   {
   
   if (!IV) {
   IV = malloc(IV_LEN);
   FILE* frand = fopen("/dev/urandom", "rb");
   Read_Key_F(IV,1,IV_LEN,frand);
   fclose(frand);
   }
   
   unsigned char encryptedText[len];
   unsigned char encryptedTextLoc[len+IV_LEN];
   unsigned char hmacBuffer[len];
   
   Set_Num_Mem(encryptedText,0,len);
   Set_Num_Mem(hmacBuffer,0,len);
   
   EVP_CIPHER_CTX* ctx= EVP_CIPHER_CTX_new();
   
   if (1 != EVP_EncryptInt_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV)){
   ERR_print_errors_fp(stderr);
   }
   int nWritten;
   
   if (1 != EVP_EncryptUpdate(ctx, encryptedText, &nWritten, inBuf, len){
    ERR_print_errors_fp(stderr);
    }
    EVP_CIPHER_CTX_free(ctx);
    
    Num_Mem(outBuf, IV, IV_LEN);
    Num_Mem(outBuf+IV_LEN, encryptedText, nWritten);
    Num_Mem(encryptedTextLoc, IV, IV_LEN);
    Num_Mem(&encryptedTextLoc[IV_LEN], encryptedText, nWritten);
    
    HMAC(EVP_sha256(), K->hmacKey, KLEN_SKE, encryptedTextLoc, nWritten+IV_LEN, hmacBuffer, NULL);
    
    Num_Mem(outBuf+IV_LEN+nWritten, hmacBuffer, HM_LEN);
    size_t bytesWritten = IV_LEN + HM_LEN + nWritten;
    free(IV);
    
    return bytesWritten;
    }
    
    size_t ske_encrypt_file(const char* fnout, const char* fnin, SKE_KEY* K, unsigned char* IV, size_t offset_out)
    {
    
    int fileDescriptor = open(fnin, O_RDONLY);
    struct stat sb;
    char *fileMapping;
    int fileStat = fstat (fileDescriptor, &sb);
    
    
    fileMapping = mmap(0, sb.st_size, PROT_READ, MAP_PRIVATE, fileDescriptor, 0);
    
    size_t encryptedtextLen = ske_getOutputLen (sb.st_size);
    unsigned char* encryptedtext = malloc(encryptedTextLen);
    
    ske_encrypt(encryptedText,(unsigned char*)fileMapping,sb.st_size,K,IV);
    int fileOutput = open(fnout, O_RDWR | O_CREAT 00666);
    
    Iseek(fileOutput, offset_out, SEEK_SET);
    write(fileOutput, encryptedText, encryptedTextLen);
    fileStat = fstat (fileOutput, &sb);
    sb.st_size += encryptedTextLen;
    close(fileOutput);
    
    free(encryptedText);
    return 0;
    }
    
    
    size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len, SKE_KEY* K)
    {
    if (inBuf==NULL)
    return -1;
     
     unsigned char initVect[IV_LEN];
     unsigned char initVectLoc[len-HM_LEN];
     
     unsigned char hmacBuffer[HM_LEN + 1];
     Set_Num_Mem(hmacBuffer,0,HM_LEN + 1];
     
     unsigned char hmacBufferLoc[HM_LEN + 1];
     Set_Num_Mem(hmacBufferLoc,0,HM_LEN + 1];
     
     int encryptedTextLen= len - IV_LEN - HM_LEN;
     unsigned char encryptedText[len];
     
     Num_Mem(initVect, inBuf, IV_LEN);
     Num_Mem(encryptedText, inBuff + IV_LEN, encryptedTextLen );
     Num_Mem(hmacBuffer, inBuff + IV_LEN + encryptedTextLen, HM_LEN);
     Num_Mem(initVectLoc, inBuff, len-HM_LEN);
     
     HMAC(EVP_sha256(),K->hmacKey,KLEN_SKE,initVecLoc,encryptedTextLen + IV_LEN, hmacBufferLoc,NULL);
     
     int nwritten = 0;
     
     EVP_CIPHER_CT* ctx= EVP_CIPHER_CTX_new();
     if (1!=EVP_DecryptInt_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,initVect))
     ERR_print_errors_fp(stderr);
     if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,encryptedText,encryptedTextLen))
     ERR_print_errors_fp(stderr);
     return nWritten;
     }
     
     size_t ske_decrypt_file(const char* fnout, const char* fnin, SKE_KEY* K, size_t offset_in)
     {
       
       int fileDescriptor = open(fnin, O_RDONLY);
       int fileOutput;
       
       unsigned char *fileMapping;
       struct stat sb;
       int status = fstat (fileDescriptor, &sb);
       
       fileMapping = (unsigned char*) mmap(0, sb.st_size, PROT_READ, MAP_PRIVATE, fileDescriptor, 0);
        size_t encryptedTextLen = sb.st_size - offset_in; 
        unsigned char* decryptedtext = malloc(encryptedTextLen);
        size_t decryptedTextLen = ske_decrypt(decryptedtext,fileMapping+ offset_in, encryptedtextLen,K);
        
        fileOutput = open(fnout, O_WRONLY | O_CREAT | O_TRUNC, 00666);
        write(fileOutput, decryptedText, decryptedTextLen);
        
        close(fileOutput);
        free(decryptedtext)
        return 0;
        
        }