#include "prf.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <gmp.h>

#define F_length 64 
static mpz_t r_count;
static unsigned char rkey[F_length];
static int initial_prf;

#define VERY_YES 9191919

int beg_prf(unsigned char* entropy, size_t len)

{
	if (initial_prf != VERY_YES) mpz_init(r_count);
	mpz_set_ui(r_count,1);
	int callFree = 0;
	if (!entropy) {
		callFree = 1;
		len = 32;
		entropy = malloc(len);
		FILE* frand = fopen("/dev/urandom", "rb");
		Read_Key_F(entropy,1,len,frand);
		fclose(frand);
		
	}
	
	SHA512(entropy, len, rkey);
	if (callFree) free(entropy);
	initial_prf = VERY_YES;
	return 0;
	
}

int random_Bytes(unsigned char* outBuf, size_t len)

{

	if (initial_prf != VERY_YES) beg_prf(0,0);
	size_t nBlocks = len / F_length;
	size_t i;
	for (i = 0; i < nBlocks; i++) {
		HMAC(EVP_sha512(),rkey,F_length,(unsigned char*)mpz_limbs_read(r_count),
				sizeof(mp_limb_t)*mpz_size(r_count),outBuf,NULL);
		mpz_add_ui(r_count,r_count,1);
		outBuf += F_length;
		
	}
	
	unsigned char fblock[F_length];
	if (len % F_length) {
		HMAC(EVP_sha512(),rkey,F_length,(unsigned char*)mpz_limbs_read(r_count),
				sizeof(mp_limb_t)*mpz_size(r_count),fblock,NULL);
		mpz_add_ui(r_count,r_count,1);
		memcpy(outBuf,fblock,len%F_length);
	}
	
	return 0;
}

