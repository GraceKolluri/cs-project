#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

#include <openssl/hmac.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <gmp.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define fn_Length 255

enum modes {
	_ENCRYPT,
	_DECRYPT,
	_GENERATE
};


#define Length_H 32 

#define Decrypt_NumK "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K){

	
	// Create Random bytes
	size_t S_Length = rsa_numBytesN(K);
	unsigned char* plain_Text = malloc(S_Length);
	unsigned char* cipher_Text = malloc(S_Length);
	plain_Text[S_Length-1] = 0;

	random_Bytes(plain_Text,S_Length-1);

	// Encrypt Random bytes.
	size_t cipher_Text_Length = rsa_encrypt(cipher_Text, plain_Text, S_Length, K);
	unsigned char _hash[Length_H];

	//SHA256 
	SHA256(plain_Text, S_Length, _hash);

	size_t Enc_Length = S_Length + Length_H;
	unsigned char* Encap_l = malloc(Enc_Length);
	Num_Mem(Encap_l,cipher_Text,S_Length);
	Num_Mem(Encap_l + S_Length,_hash,Length_H);

	FILE *f = fopen(fnOut, "w+");
	fwrite(Encap_l, Enc_Length, 1, f);
	fclose(f);

	//SKE_KEY 
	SKE_KEY Key_Decrypt;
	ske_keyGen(&Key_Decrypt,plain_Text,S_Length);
	ske_encrypt_file(fnOut,fnIn,&Key_Decrypt,NULL,Enc_Length);

	free(plain_Text); free(cipher_Text); free(Encap_l);

	return 0;
}

int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K){

	FILE *f = fopen(fnIn, "r");
	unsigned char c[Length_H + 1];
	c[Length_H] = 0;
	
	size_t m_len = rsa_numBytesN(K);
	unsigned char* encap = malloc(m_len + Length_H);
	
	Read_Key_F(encap, m_len + Length_H, 1, f);
	fclose(f);

	unsigned char* Random_Gen_Encrypt = malloc(m_len);
	Num_Mem(Random_Gen_Encrypt, encap, m_len);
	
	unsigned char* Random_Gen_Decrypt = malloc(m_len);
	rsa_decrypt(Random_Gen_Decrypt,Random_Gen_Encrypt,m_len,K);

	Num_Mem(c, encap + m_len, Length_H);

	unsigned char HASH[Length_H + 1];
	HASH[Length_H] = 0;

	// Create SHA256 
	SHA256(Random_Gen_Decrypt, m_len, HASH);

	// Create SKE_KEY 
	SKE_KEY Key_Encrypt;
	ske_keyGen(&Key_Encrypt,Random_Gen_Decrypt,m_len);
	ske_decrypt_file(fnOut,fnIn,&Key_Encrypt,m_len + Length_H);

	return 0;
}


int main(int argc, char *argv[]) {


	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	
	char c;
	int opt_index = 0;
	char fnRnd[fn_Length+1] = "/dev/urandom";
	fnRnd[fn_Length] = 0;
	char fnIn[fn_Length+1];
	char fnOut[fn_Length+1];
	char fnKey[fn_Length+1];
	Set_Num_Mem(fnIn,0,fn_Length+1);
	Set_Num_Mem(fnOut,0,fn_Length+1);
	Set_Num_Mem(fnKey,0,fn_Length+1);
	int mode = _ENCRYPT;

	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,fn_Length);
				break;
			case 'o':
				strncpy(fnOut,optarg,fn_Length);
				break;
			case 'k':
				strncpy(fnKey,optarg,fn_Length);
				break;
			case 'r':
				strncpy(fnRnd,optarg,fn_Length);
				break;
			case 'e':
				mode = _ENCRYPT;
				break;
			case 'd':
				mode = _DECRYPT;
				break;
			case 'g':
				mode = _GENERATE;
				strncpy(fnOut,optarg,fn_Length);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	
	RSA_KEY K;
	rsa_initKey(&K);
	FILE* f_Public;
	FILE* f_Private;
	char* fPub = malloc(10);    
	char* fPrvt = malloc(10);   

	switch (mode) 
	{
		case _ENCRYPT:
			
			f_Public = fopen(fnKey, "r");
			rsa_readPublic(f_Public, &K);
			fclose(f_Public);
			kem_encrypt(fnOut, fnIn, &K);

			break;

		case _DECRYPT:
			
			f_Private = fopen (fnKey, "r");
			rsa_readPrivate(f_Private, &K);
			fclose(f_Private);
			kem_decrypt(fnOut, fnIn, &K);

			break;

		case _GENERATE:

			rsa_generate_key(nBits, &K);
			f_Private = fopen (fnOut, "w+");
			rsa_writePrivate(f_Private, &K);		
			fclose(f_Private);

			snprintf(fnIn, fn_Length, "%s.pub", fnOut);
		
			f_Public = fopen (fnIn, "w+");
			rsa_writePublic(f_Public, &K);
			fclose(f_Public);

			break;

		default:

			return 1;
	}

	rsa_shredKey(&K);
	

	return 0;
}
