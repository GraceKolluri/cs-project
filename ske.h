#pragma once

#include <openssl/aes.h>
#include <inttypes.h>
#include <stdio.h>

#define KLEN_SKE 32

typedef struct _SKE_KEY {
	unsigned char hmacKey[KLEN_SKE];
	unsigned char aesKey[KLEN_SKE];
} SKE_KEY;

size_t ske_getOutputLen(size_t inputLen);

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen);
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV);
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K);
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out);

size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in);
